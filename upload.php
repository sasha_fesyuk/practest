<?php
//connect to configuration file
include 'config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Upload status</title>
    </head>
    <body>
        <?php
        /*$servername = "localhost";
        $username = "root";
        $password = "12345";
        $dbname = "practest";
         * 
         */
        $mysqli = new mysqli($servername, $username, $password, $dbname);
        
        //Checking Connection
        if ($mysqli->connect_error){
            $mysqli->close();
            $mysqli = new mysqli($servername, $username, $password);
            $db_query = 'CREATE DATABASE IF NOT EXISTS '.$dbname.' CHARACTER SET '.$charset.' COLLATE '.$collate.'; ';
            $db_sel = 'USE '.$dbname.'; ';
            $db_q_t= 'CREATE TABLE IF NOT EXISTS '.$dbname.' (
                id     INT AUTO_INCREMENT PRIMARY KEY,
                path VARCHAR (400) DEFAULT NULL,
                name VARCHAR (300) DEFAULT NULL,
                size DOUBLE,
                note VARCHAR (400) 
            ); ';
            $db_query .=$db_sel.$db_q_t;
            if ($mysqli->multi_query($db_query) === FALSE) {
                echo "Error: " . $db_query . "<br>" . $mysqli->error;
            }
            $mysqli->close();
            $mysqli = new mysqli($servername, $username, $password, $dbname);
        }
        
        $target_dir = "uploads/";
        if (!file_exists($target_dir)) {
          mkdir($target_dir, 0777, true);
        }
        if(count($_FILES["fileToUpload"]["name"]) > 0) {
            $i = 0; 
            echo '<ul>';
            foreach($_FILES["fileToUpload"]["name"] as $filename) { 
                echo '<li>';
                $f_name = $_FILES["fileToUpload"]["name"][$i];
                $target_file = $target_dir . $f_name;
                $uploadOk = 1;
                $db_q_i = '';
                // Check if file already exists
                if (file_exists($target_file)) {
                    echo "Sorry, file ".$f_name. " already exists.  Plaese, <a href=\"index.php\">upload other file</a>. ";
                    $uploadOk = 0;
                }
                // Check file size
                elseif ($_FILES["fileToUpload"]["size"][$i] > 500000) { // ~500 KB
                    echo "Sorry, your file is too large.  Plaese, <a href=\"index.php\">upload other file</a>. ";
                    $uploadOk = 0;
                }
                // Check if $uploadOk is set to 0 by an error
                elseif ($uploadOk == 0) {
                    echo "Sorry, your file was not uploaded. Plaese, <a href=\"index.php\">upload new file</a>. ";
                // if everything is ok, try to upload file
                } else {
                    $result = move_uploaded_file($_FILES["fileToUpload"]["tmp_name"][$i], $target_file);
                    if ($result===true) {
                        echo "The file \"". $f_name . "\" has been uploaded. ";
                        $fsize = filesize($target_file); // bytes
                        $fsize = round($fsize / 1024, 2); // kilobytes with two digits
                        $db_q_i = "INSERT INTO ".$dbname." (path, name, size) VALUES (N'".$target_file."',N'".$f_name."',N'".$fsize."'); ";
                        if ($mysqli->multi_query($db_q_i) === FALSE) {
                            echo "Error: " . $db_q_i . "<br>" . $mysqli->error;
                        }
                    } else {
                        echo "Sorry, there was an error uploading your file, please <a href=\"index.php\">try again.</a> ";
                    }
                }
                echo '</li>';
               $i++;
            }
            echo '</ul>';
        }
        
        
        
        $mysqli->close();
        ?>
        <br/><a href="index.php">Upload new file</a> or see <a href="list.php">list of files</a>.
    </body>
</html>
