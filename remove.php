<?php
//connect to configuration file
include 'config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <!--meta charset="UTF-8" http-equiv="refresh" content="0; URL=/list.php"-->
        <meta charset="UTF-8">
        <title>File deleting</title>
    </head>
    <body>
        <?php
        if(isset($_REQUEST["id"])){
            // Get parameters
            $id = $_REQUEST["id"];
            //connect to the db
            $mysqli = new mysqli($servername, $username, $password, $dbname);
            if (!$mysqli->set_charset($charset)) {
                printf("Error of utf8: %s\n", $mysqli->error);
                exit();
            }
            $db_query = "SELECT path, name FROM ".$dbname." WHERE id=".$id.";";
            $result = $mysqli->query($db_query);
            if ($result === FALSE) {
                echo "Error: " . $db_query . "<br>" . $mysqli->error;
            }
            $result = $result->fetch_assoc();
            $path = $result["path"];
            $name = $result["name"];

            if (!$mysqli->connect_error){
                // Deleting file
                if (!unlink($path)) {
                    echo ("Error deleting \"".$name."\".");
                } else {
                    $db_query = "DELETE FROM ".$dbname." WHERE id=".$id.";";
                    if ($mysqli->multi_query($db_query) === FALSE) {
                        echo "Error: " . $db_query . "<br>" . $mysqli->error;
                    }
                    echo ("File \"".$name."\" is deleted.");
                }
            }
            
            $mysqli->close();
            
            //$filepath = "uploads/" . $file;
        }
        ?>
        <br/>
        <br/>
        You can see list of files <a href="list.php">there</a>. Or add new file <a href="index.php">there</a>.
    </body>
</html>