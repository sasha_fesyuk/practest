<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>File(s) uploader</title>
    </head>
<body>
<!-- The form to upload file -->
<form action="upload.php" method="post" enctype="multipart/form-data">
    Select file(s) to upload:
    <input type="file" name="fileToUpload[]" id="fileToUpload" multiple> <!--Button for select of file -->
    <input type="submit" value="Upload file(s)" name="submit"> <!-- Button to call file upload.php which will upload file -->
</form>

<br/>
<br/>
You can see list of files <a href="list.php">there</a>.
</body>
</html>