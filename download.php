<?php
if(isset($_REQUEST["file"])){
    // Get parameters
    $file = urldecode($_REQUEST["file"]); // Decode URL-encoded string
    $filepath = "uploads/" . $file;
    
    // Process download
    if(file_exists($filepath)) {
        header('Content-Description: File Transfer');//Send the file to user. https://developer.mozilla.org/ru/docs/Web/HTTP/%D0%97%D0%B0%D0%B3%D0%BE%D0%BB%D0%BE%D0%B2%D0%BA%D0%B8/Content-Disposition
        header('Content-Type: application/octet-stream'); //binary file. Or we can say that is image/png: "Content-Type: image/png"
        header('Content-Disposition: attachment; filename="'.$file.'"'); //Show the name in form, for load file
        header('Expires: 0'); //Contains the date/time after which the response is considered stale. https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Expires
        header('Cache-Control: no-cache, no-store, must-revalidate'); //https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control
        header('Pragma: public'); //Standart. Associated with caching. https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Pragma
        header('Content-Length: ' . filesize($filepath)); //Size of the entity-body, in bytes, sent to the recipient.
        flush(); // Clean system output buffer and send data to brouser of user.
        readfile($filepath); //The readfile() function reads a file and writes it to the output buffer.
        exit; // Exit from "transfer procces"
    }
}
?>
