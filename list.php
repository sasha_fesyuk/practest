<?php
//connect to configuration file
include 'config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">

        <title>List of files</title>
    </head>
    <style>
        table, th, td {
            border: 1px solid black; 
            border-collapse: collapse;
            padding: 2px 10px;
        }
        .row0 {
            background-color: rgb(225, 0, 0, 0.3);
        }
        .row1 {
            background-color: rgb(0, 255, 0, 0.3);
        }
        .download, .delete {
            text-align: center;
        }
    </style>
    <body>
        <?php
        //connect to the db
        $mysqli = new mysqli($servername, $username, $password, $dbname);
        if (!$mysqli->connect_error){
            /* set charset utf8 */
            if (!$mysqli->set_charset($charset)) {
                printf("Error of utf8: %s\n", $mysqli->error);
                exit();
            }
            $db_query = "SELECT id, path, name, size FROM ".$dbname.";";
            $result = $mysqli->query($db_query);
            
            if ($result->num_rows > 0) {
                // output data of each row
                $i=1;
                ?>
                <table id="uploadTable">
                    <thead  class="row0">
                       <th>#</th><th>name</th><th>size</th><th>download</th><th>delete</th>
                    </thead>
                    <tbody>
                <?php
                while($row = $result->fetch_assoc()) {
                    echo "<tr class=\"row".($i % 2)."\">";
                    echo "<td>".$i."</td>";
                    $file = $row["name"];
                    echo "<td>".$file."</td>";
                    echo "<td>".$row["size"]." KB </td>";
                    $path = $row["path"];
                    echo "<td class=\"download\"><a href=\"download.php?file=".urlencode($file)."\"><img src=\"Downloads-icon.png\" alt=\"Download file".$f."\"></a></td>";
                    echo "<td class=\"delete\"><a href=\"remove.php?id=".$row["id"]."\"><img src=\"Actions-edit-delete-icon.png\" alt=\"Delete file".$f."\"></a></td>";
                    echo "</tr>";
                    $i++;
                    //echo "Path: " . $row["path"]. " name" . $row["name"]. " size" . $row["size"]. "<br>";
                }
                ?>
                    </tbody>
                </table>
                <br /><a href="index.php">Upload new file(s)</a>.
                <?php
            } else {
                echo "<br/> Sorry, directory is empty. Plaese, <a href=\"index.php\">upload new file</a>. ";
            }
             $mysqli->close();
        } else {
            echo "Can not connect to database. <a href=\"index.php\">Upload new file</a>.";
        }
        /*
        $dir = "uploads/";
        if (file_exists($dir)) {
            // Sort in ascending order - this is default
            $lofs = scandir($dir); //list of files
            $i=0;
            ?>
            <table id="uploadTable">
              <thead>
                <th>#</th><th>name</th><th>size</th><th>download</th><th>delete</th>
              </thead>
            <tbody>
            <?php
            foreach ($lofs as $f) {
                if($i > 1) {
                    echo "<tr>";
                    echo "<td>".($i-1)."</td>";
                    echo "<td>$f</td>";
                    $fsize = filesize($dir.$f); // bytes
                    $fsize = round($fsize / 1024, 2); // kilobytes with two digits
                    echo "<td>$fsize KB </td>";
                    echo "<td class=\"download\"><a href=\"download.php?file=".urlencode($f)."\"><img src=\"Downloads-icon.png\" alt=\"Download file".$f."\"></a></td>";
                    echo "<td class=\"delete\"><a href=\"remove.php?file=".urlencode($f)."\"><img src=\"Actions-edit-delete-icon.png\" alt=\"Delete file".$f."\"></a></td>";
                    echo "</tr>";
                }
                $i++;
            }
            ?>
              </tbody>
            </table>
            <?php
            if ($i<=2) {
                echo "<br/> Sorry, directory is empty. Plaese, <a href=\"index.php\">upload new file</a>. ";
            }
        } else {
            echo "<br/> Sorry, directory does not existe. You can upload new file <a href=\"index.php\">here</a>";
        }
        */
        ?>

    </body>
</html>
